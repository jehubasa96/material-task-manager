package com.example.jayze.materialtaskmgrv1;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    Button add,update, delete;
    DataBaseHelper db;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        db=new DataBaseHelper(getActivity());
        View rootView=inflater.inflate(R.layout.fragment_home, container, false);

        //When the Add Button is clicked
        add=(Button)rootView.findViewById(R.id.addButton);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog=new Dialog(getActivity());
                dialog.setTitle("adding form");
                dialog.setContentView(R.layout.add_form);
                dialog.show();

                final EditText taskTitle=(EditText)dialog.findViewById(R.id.taskTitleAdd);
                final EditText taskDesc=(EditText)dialog.findViewById(R.id.taskDescriptionAdd);
                Button submitButton=(Button)dialog.findViewById(R.id.submitButtonAdd);
                Button cancelButton=(Button)dialog.findViewById(R.id.cancelButtonAdd);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String title=taskTitle.getText().toString();
                        String description=taskDesc.getText().toString();
                        dialog.cancel();
                        boolean test=db.insertData(title,description);
                        if(test==true){
                            db.getData();
                            Toast.makeText(getActivity(),"Data is added",Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(getActivity(),"Data is not recorded",Toast.LENGTH_SHORT).show();
                    }
                });
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        //When the Update Button is clicked
        update=(Button)rootView.findViewById(R.id.updatebutton);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog=new Dialog(getActivity());
                dialog.setTitle("Updating Form");
                dialog.setContentView(R.layout.update_form);
                dialog.show();

                final EditText taskNum=(EditText)dialog.findViewById(R.id.itemNumUpdate);
                final EditText taskTitle=(EditText)dialog.findViewById(R.id.taskTitleUpdate);
                final EditText taskDesc=(EditText)dialog.findViewById(R.id.taskDescriptionUpdate);
                Button submitButton=(Button)dialog.findViewById(R.id.submitButtonUpdate);
                Button cancelButton=(Button)dialog.findViewById(R.id.cancelButtonUpdate);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String num=taskNum.getText().toString();
                        String title=taskTitle.getText().toString();
                        String description=taskDesc.getText().toString();
                        dialog.cancel();
                        boolean test=db.updateData(num,title,description);
                        if(test==true){
                            Toast.makeText(getActivity(),"Data is added",Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(getActivity(),"Data is not recorded",Toast.LENGTH_SHORT).show();
                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        //When the Delete Button is clicked
        delete=(Button)rootView.findViewById(R.id.deleteButton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog= new Dialog(getActivity());
                dialog.setTitle("Deleting Form");
                dialog.setContentView(R.layout.delete_form);
                dialog.show();

                final EditText taskNum=(EditText)dialog.findViewById(R.id.itemNumDelete);
                Button submitButton=(Button)dialog.findViewById(R.id.submitButtonDelete);
                Button cancelButton=(Button)dialog.findViewById(R.id.cancelButtonDelete);
                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String num=taskNum.getText().toString();
                        int integer = db.deleteData(num);
                        dialog.cancel();
                        if(integer>0){
                            Toast.makeText(getActivity(),"Data is deleted",Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(getActivity(),"Data is not deleted",Toast.LENGTH_SHORT).show();
                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        // Inflate the layout for this fragment
        return rootView;


    }

}
