package com.example.jayze.materialtaskmgrv1;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleList extends Fragment {
    DataBaseHelper db;
    ListView list;
    public ScheduleList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_schedule_list, container, false);
        list=(ListView)rootView.findViewById(R.id.listView);
        db=new DataBaseHelper(getActivity());
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible){
            Cursor cursor=db.getData();
            String [] fromFieldNames=new String[]{db.Col1,db.Col2,db.Col3};
            int [] toView=new int[]{R.id.itemNum,R.id.textTop,R.id.subItem};
            SimpleCursorAdapter simpleCursorAdapter=new SimpleCursorAdapter(getContext(),R.layout.item_layout,
                    cursor,fromFieldNames,toView,0);
            list.setAdapter(simpleCursorAdapter);

        }
    }
}
