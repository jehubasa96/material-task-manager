package com.example.jayze.materialtaskmgrv1;

import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jayze on 08/03/2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DataBase_Name="Task.db";
    public static final String Table_Name="tasktable";
    public static final String Col1 ="_id";
    public static final String Col2="title";
    public static final String Col3="description";

    public static final String[] allKeys =new String[]{Col1, Col2, Col3};

    public DataBaseHelper(Context context) {
        super(context, DataBase_Name, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Table_Name + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST "+Table_Name);
        onCreate(db);
    }

    public boolean insertData(String title, String description){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(Col2,title);
        contentValues.put(Col3, description);
        long result=db.insert(Table_Name,null,contentValues);
        if(result==-1){
            return false;
        }
        else
            return true;
    }

    public boolean updateData(String id, String title, String description){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(Col1 ,id);
        contentValues.put(Col2,title);
        contentValues.put(Col3, description);
        db.update(Table_Name,contentValues,"_id = ?", new String []{id});
        return true;
    }

    public Integer deleteData(String id){
        SQLiteDatabase db=this.getWritableDatabase();
        return db.delete(Table_Name, "_id = ?", new String []{id});
    }

    public Cursor getData() {
        SQLiteDatabase db=this.getWritableDatabase();
        String where=null;
        Cursor cursor= db.query(true, Table_Name,allKeys,where,null,null,null,null,null);
        if(cursor!=null){
            cursor.moveToFirst();
        }
        return cursor;
    }
}
